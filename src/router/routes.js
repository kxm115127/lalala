
//  菜单路由 包含三级 以及 只有二级 和 一级的
const login = () => import('@/pages/login')
const news1 = () => import('@/pages/news1')
const news2 = () => import('@/pages/news2')
const news3 = () => import('@/pages/news3')
const news4 = () => import('@/pages/news4')
const product1 = () => import('@/pages/product1')
const product2 = () => import('@/pages/product2')
const product3 = () => import('@/pages/product3')
const setting = () => import('@/pages/setting')
const user1 = () => import('@/pages/user1')
const user2 = () => import('@/pages/user2')
const notfound = () => import('@/pages/notfound')

export default [
  {
    path: '*',
    redirect: '/404',
    hidden: true
  },
  {
    path: '/',
    redirect: '/news1',
    hidden: true
  },
  {
    path: '/news1',
    name: '新闻1',
    icon: 'el-icon-menu',
    components: {
      default: news1
    },
    children: [
      {
        path: '/news2',
        name: '新闻2',
        components: {
          default: news2
        },
        children: [
          {
            path: '/news3',
            name: '新闻3',
            components: {
              default: news3
            }
          }
        ]
      },
      {
        path: '/news4',
        name: '新闻4',
        components: {
          default: news4
        }
      }
    ]
  },
  {
    path: '/producjt1',
    name: '产品1',
    icon: 'el-icon-menu',
    components: {
      default: product1
    },
    children: [
      {
        path: '/producjt2',
        name: '产品2',
        components: {
          default: product2
        },
        children: [
          {
            path: '/producjt3',
            name: '产品3',
            components: {
              default: product3
            }
          }
        ]
      }
    ]
  },
  {
    path: '/user1',
    name: '用户1',
    icon: 'el-icon-menu',
    components: {
      default: user1
    },
    children: [
      {
        path: '/user2',
        name: '用户2',
        components: {
          default: user2
        }
      }
    ]
  },
  {
    path: '/setting',
    name: '设置',
    icon: 'el-icon-menu',
    components: {
      default: setting
    }
  },
  {
    path: '/login',
    name: '登录',
    hidden: true,
    components: {
      default: login
    }
  },
  {
    path: '/404',
    name: '找不到页面',
    hidden: true,
    components: {
      default: notfound
    }
  }
]
